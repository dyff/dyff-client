# coding=utf-8
# --------------------------------------------------------------------------
# Code generated by Microsoft (R) AutoRest Code Generator (autorest: 3.10.2, generator: @autorest/python@6.27.1)
# Changes may cause incorrect behavior and will be lost if the code is regenerated.
# --------------------------------------------------------------------------
# pylint: disable=wrong-import-position

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from ._patch import *  # pylint: disable=unused-wildcard-import

from ._operations import DatasetsOperations  # type: ignore
from ._operations import EvaluationsOperations  # type: ignore
from ._operations import InferenceservicesOperations  # type: ignore
from ._operations import InferencesessionsOperations  # type: ignore
from ._operations import MeasurementsOperations  # type: ignore
from ._operations import MethodsOperations  # type: ignore
from ._operations import ModelsOperations  # type: ignore
from ._operations import ModulesOperations  # type: ignore
from ._operations import ReportsOperations  # type: ignore
from ._operations import SafetycasesOperations  # type: ignore
from ._operations import TokensOperations  # type: ignore
from ._operations import UsecasesOperations  # type: ignore
from ._patch import __all__ as _patch_all
from ._patch import patch_sdk as _patch_sdk

__all__ = [
    "TokensOperations",
    "DatasetsOperations",
    "EvaluationsOperations",
    "InferenceservicesOperations",
    "InferencesessionsOperations",
    "MeasurementsOperations",
    "MethodsOperations",
    "ModelsOperations",
    "ModulesOperations",
    "UsecasesOperations",
    "ReportsOperations",
    "SafetycasesOperations",
]
__all__.extend([p for p in _patch_all if p not in __all__])  # pyright: ignore
_patch_sdk()
